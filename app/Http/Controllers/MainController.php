<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\User;
use Hash;

class MainController extends Controller
{
    function index() {
        return view('login');
    }

    function checkLogin(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:3'
        ]);

        $user_data = array(
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        );

        if(Auth::attempt($user_data)) {
            return redirect('/loginComplete');
        }
        else {
            return back()->with('error', 'Your login details do not match');
        }
    }

    function loginComplete() {
        return view('loginComplete');
    }

    function logout() {
        Auth::logout();
        return redirect('/');
    }

    function register() {
        return view('register');
    }

    function registerUser(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:3'
        ]);

        $user = new User();

        $user->name = $request ->name;
        $user->email = $request ->email;
        $user->password = Hash::make($request ->password);

        $user->save();

        return view('login');
    }
}
